FROM golang:1.16-alpine AS build
ENV GO111MODULE=on \
  CGO_ENABLED=0 \
  GOOS=linux \
  GOARCH=amd64
WORKDIR /src
COPY ./src .
RUN apk add git &&go mod download
RUN go get -t && go build -o /exporter .
RUN ls -l /
FROM alpine:3.13
USER nobody
COPY --from=build /exporter /exporter
CMD /exporter
EXPOSE 8090
