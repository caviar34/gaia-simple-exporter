package main

import (
	"encoding/json"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"
)

type NetInfo struct{
	Result struct {
		NumPeers string `json:"n_peers"`
	} `json:"result"`
}
type Status struct {
	Result struct{
		SyncInfo struct{
			LatestBlockHeight string `json:"latest_block_height"`
			LatestBlockTime string `json:"latest_block_time"`
		} `json:"sync_info"`
	} `json:"result"`
}

func fetch_http_data(url string)[]byte{
	GaiaClient := http.Client{
		Timeout: 5* time.Second,
	}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatal(err)
	}
	res, err := GaiaClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	if res.Body != nil {
		defer res.Body.Close()
	}
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}
	return body
}

func get_latest_block_height(data []byte)float64 {
	status := Status{}
	err := json.Unmarshal(data, &status)
	if err != nil {
		log.Fatal(err)
	}
	r, err := strconv.ParseFloat(status.Result.SyncInfo.LatestBlockHeight, 64)
	if err != nil {
		log.Fatal(err)
	}
	return r
}

func get_block_time_discrepancy(data []byte)float64 {
	status := Status{}
	err := json.Unmarshal(data, &status)
	if err != nil {
		log.Fatal(err)
	}
	latest_block_time, err := time.Parse(time.RFC3339Nano, status.Result.SyncInfo.LatestBlockTime)
	return 	float64(time.Now().Unix()-latest_block_time.Unix())
}

func get_peers_num(data []byte)float64 {
	status := NetInfo{}
	err := json.Unmarshal(data, &status)
	if err != nil {
		log.Fatal(err)
	}
	r, err := strconv.ParseFloat(status.Result.NumPeers, 64)
	if err != nil {
		log.Fatal(err)
	}
	return r
}

func recordMetrics() {
	go func() {
		var status_data []byte
		for {
			status_data=fetch_http_data("http://127.0.0.1:26657/status")
			currentBlockNumber.Set(get_latest_block_height(status_data))
			numPeers.Set(get_peers_num(fetch_http_data("http://127.0.0.1:26657/net_info")))
			latestBlockTimeDiscrepancy.Set(get_block_time_discrepancy(status_data))
			time.Sleep(2 * time.Second)
		}
	}()
}

var (
	opsProcessed = promauto.NewCounter(prometheus.CounterOpts{
		Name: "myapp_processed_ops_total",
		Help: "The total number of processed events",
	})
	currentBlockNumber = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "gaia_latest_block_height",
		Help: "Block number under gaiad:26657/status|jq .result.sync_info.latest_block_height",
	})
	numPeers = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "gaia_num_peers",
		Help: "Num peers under http://gaiad:26657/net_info|jq .result.n_peers",
	})
	latestBlockTimeDiscrepancy = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "gaia_latest_block_time_discrepancy",
		Help: "Difference between now and 26657/status|jq .result.sync_info.latest_block_time",
	})
)

func main() {
	//data:=fetch_http_data("185.189.68.19:26657/status")
	recordMetrics()
	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(":8090", nil)
}
